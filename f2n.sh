#!/bin/bash
# Check operation system
VERSION=$(grep -oP '(?<=^VERSION_ID=).+' /etc/os-release | tr -d '"')
# Check operation system version
OS=$(grep -oP '(?<=^ID=).+' /etc/os-release | tr -d '"')
cd sc
chmod +x f2b*.sh
if (("$OS" = ubuntu ))
then
  echo " $OS $VERSION "
  sleep 3s
  ./f2b.sh
else
  if (( "$VERSION" < 12 ))
  then
    echo "$OS $VERSION"
    sleep 3s
    ./f2b_pve7.sh
  else
  echo "$OS $VERSION"
  sleep 3s
  ./f2b_pve8.sh
  fi
fi