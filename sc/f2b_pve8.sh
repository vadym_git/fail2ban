#!/bin/bash
apt remove fail2ban -y
apt purge fail2ban -y
rm -r /etc/fail2ban
apt autoclean -y
apt autopurge -y
apt install fail2ban -y

echo "[sshd]
enabled = true" >> /etc/fail2ban/jail.local

echo "[DEFAULT]
## initial ban time:
bantime = 1h
## incremental banning:
bantime.increment = true
## default factor (causes increment - 1h -> 1d 2d 4d 8d 16d 32d ...):
bantime.factor = 24
## max banning time = 5 week:
bantime.maxtime = 5w
## A host is banned if it has generated "maxretry" during the last "findtime"
## seconds.
findtime  = 30m
## "maxretry" is the number of failures before a host get banned.
maxretry = 3
## send notification to emsil
#destemail = email_resiver
#sender = email_sender
#ction = %(action_mw)s
" >> /etc/fail2ban/jail.local

echo "[DEFAULT]
banaction = nftables
banaction_allports = nftables[type=allports]" >> /etc/fail2ban/jail.local

echo "[sshd]
enabled   = true
filter    = sshd
banaction = iptables
backend   = systemd
maxretry  = 3
findtime  = 1d
bantime   = 2w
ignoreip  = 127.0.0.1/8" >> /etc/fail2ban/jail.d/sshd.local

echo "[Definition]
failregex = ^.*DROP_.*SRC=<ADDR> DST=.*$
journalmatch = _TRANSPORT=kernel" >> /etc/fail2ban/filter.d/fwdrop.local

echo "[proxmox]
enabled = true
port = https,http,8006
filter = proxmox
banaction = iptables-multiport
backend   = systemd
maxretry  = 3
findtime  = 1d
bantime   = 2w
" >> /etc/fail2ban/jail.conf

echo "[Definition]
failregex = pvedaemon\[.*authentication (verification )?failure; rhost=<HOST> user=.* msg=.*
ignoreregex =
" >> /etc/fail2ban/filter.d/proxmox.conf

## change default ssh port
read -p 'Enter port for ssh connection  :' SSH_Port
echo "$SSH_Port"
sed -i "s/port    = ssh/port    = $SSH_Port/" /etc/fail2ban/jail.conf
sed -i "s/port     = ssh/port     = $SSH_Port/" /etc/fail2ban/jail.conf
sed -i "s/port     = ssh/port     = $SSH_Port/" /etc/fail2ban/jail.conf

systemctl restart fail2ban

service fail2ban status
