#!/bin/bash
service fail2ban stop
apt remove fail2ban -y
apt purge fail2ban -y
rm -r /etc/fail2ban
apt autoclean -y
apt autopurge -y
apt install fail2ban -y

touch /etc/fail2ban/filter.d/proxmox.conf
echo "[Definition]
failregex = pvedaemon\[.*authentication failure; rhost=<HOST> user=.* msg=.*
ignoreregex =" >> /etc/fail2ban/filter.d/proxmox.conf

echo "
[proxmox]
enabled = true
port = https,http,8006
filter = proxmox
logpath = /var/log/daemon.log
maxretry = 3
## 1 hour
bantime = 3600" >> /etc/fail2ban/jail.conf

echo "[DEFAULT]
## initial ban time:
bantime = 1h
## incremental banning:
bantime.increment = true
## default factor (causes increment - 1h -> 1d 2d 4d 8d 16d 32d ...):
bantime.factor = 24
## max banning time = 5 week:
bantime.maxtime = 5w
## A host is banned if it has generated "maxretry" during the last "findtime"
## seconds.
findtime  = 30m
## "maxretry" is the number of failures before a host get banned.
maxretry = 3
## send notification to emsil
#destemail = email_resiver
#sender = email_sender
#action = %(action_mw)s
" >> /etc/fail2ban/jail.local

## change default ssh port
read -p 'Enter port for ssh connection  :' SSH_Port
echo "$SSH_Port"
sed -i "s/port    = ssh/port    = $SSH_Port/" /etc/fail2ban/jail.conf
sed -i "s/port     = ssh/port     = $SSH_Port/" /etc/fail2ban/jail.conf
sed -i "s/port     = ssh/port     = $SSH_Port/" /etc/fail2ban/jail.conf

systemctl restart fail2ban

service fail2ban status
